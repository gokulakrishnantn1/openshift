var img1 = 0,
    img2 = 1,
    img,
    images = ["img1.jpeg", "img2.jpg", "img3.jpeg", "img4.jpg", "img5.jpg", "img6.jpeg", "img7.jpg"];
document.getElementById("img1").src = images[0];
document.getElementById("img2").src = images[1];

function img1Click() {
    if (img1 < img2)
        img1 = img2 + 1;
    else
        img1++;
    if (img1 == images.length) {
        img = images[img2];
        end("img2", img);
    } else
        document.getElementById("img1").src = images[img1];
}

function img2Click() {
    if (img2 < img1)
        img2 = img1 + 1;
    else
        img2++;
    if (img2 == images.length) {
        img = images[img1];
        end("img1", img);
    } else
        document.getElementById("img2").src = images[img2];
}

function end(index, image) {
    if (index == "img2") {
        document.getElementById("img1").style.display = "none";
        document.getElementById("img2").removeAttribute("onclick");
    } else {
        document.getElementById("img2").style.display = "none";
        document.getElementById("img1").removeAttribute("onclick");
    }
    $.get('/PM/' + image, function (data, status) {
        document.getElementById("top").innerHTML = "Thank you for your choice !";
        document.getElementById("hint").innerHTML = "Nice one...<br>" + data.count + " out of " + data.total + " have chosen this pic.";
    });
}

$(window).resize(function () {
    if ($(window).width() / 1280 < $(window).height() / 934) {
        document.body.style.zoom = "" + ($(window).width() / 1280) * 100 + "%";
    } else {
        document.body.style.zoom = "" + ($(window).height() / 934) * 100 + "%";
    }
});

if ($(window).width() != 1280 || $(window).height() != 934)
    if ($(window).width() / 1280 < $(window).height() / 934) {
        document.body.style.zoom = "" + ($(window).width() / 1280) * 100 + "%";
    } else {
        document.body.style.zoom = "" + ($(window).height() / 934) * 100 + "%";
    }
