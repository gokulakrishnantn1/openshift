var fs = require('fs');
var express = require('express');
var app = express();
var time = new Date();
var space = "                             ";
var user = [],
    flag;

var mysql = require("mysql");

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "nfsmw",
    database: "Sample"
});

con.connect(function (err) {
    if (err) {
        console.log('Error connecting to Db\n');
        return;
    } else {
        console.log('Connection established\n');
    }
});

/*con.end(function (err) {
    console.log("End");
});*/

function requestFrom(req) {
    if (req.connection.remoteAddress != '192.168.1.199' && req.connection.remoteAddress != '192.168.1.122' && req.connection.remoteAddress != '127.0.0.1') {
        fs.readFile('./Request.txt', function read(err, data) {
            if (err) {
                console.log(err);
            } else {
                time = new Date();
                fs.writeFile("./Request.txt", data + String(req.connection.remoteAddress + space).slice(0, 15) + " - " + String(req.url + space).slice(0, 20) + " - " + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds() + "\n", function (err) {
                    if (err) {
                        console.log(err);
                    }
                });
            }
        });
    }
}

function Console(req) {
    if (req.connection.remoteAddress != '192.168.1.199' && req.connection.remoteAddress != '192.168.1.122' && req.connection.remoteAddress != '127.0.0.1')
        console.log(req.connection.remoteAddress + " - " + req.url);
    flag = 1;
    for (var i = 0; i < user.length; i++)
        if (user[i] == req.connection.remoteAddress)
            flag = 0;
    if (req.connection.remoteAddress == '192.168.1.199' || req.connection.remoteAddress == '192.168.1.122')
        flag = 0;
    if (flag) {
        user.push(req.connection.remoteAddress);
        fs.writeFile("./Users.txt", user.toString().replace(/,/gi, "\n"), function (err) {
            if (err) {
                return console.log(err);
            }
        });
    }
}

app.get('/', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Home.html');
    requestFrom(req);
});

app.get('/favicon.ico', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/favicon.ico');
});

app.get('/Car.jpg', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Car.jpg');
    requestFrom(req);
});

app.get('/Audio1.mp3', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Audio1.mp3');
    requestFrom(req);
});

app.get('/Audio2.mp3', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Audio2.mp3');
    requestFrom(req);
});

app.get('/Request.txt', function (req, res) {
    res.sendFile(__dirname + '/Request.txt');
    Console(req);
});

app.get('/Users.txt', function (req, res) {
    res.sendFile(__dirname + '/Users.txt');
    Console(req);
});

app.get('/Again.png', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Again.png');
});

app.get('/bootstrap.min.css', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/bootstrap.min.css');
});

app.get('/bootstrap.min.js', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/bootstrap.min.js');
});

app.get('/Game.html', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Game.html');
    con.query('INSERT IGNORE INTO Users SET ?', {
        ip: req.connection.remoteAddress.toString(),
        best: 0,
        network_best: 0
    }, function (err, rows) {
        if (err) console.log(err);
    });
});

app.get('/Game.css', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Game.css');
});

app.get('/Game.js', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Game.js');
});

app.get('/Game.png', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Game.png');
});

app.get('/jquery-2.2.4.js', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/jquery-2.2.4.js');
});

app.get('/Picmash.css', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Picmash.css');
});

app.get('/Picmash.html', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Picmash.html');
});

app.get('/Picmash.js', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Picmash.js');
});

app.get('/Picmash.png', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Picmash.png');
});

app.get('/ReadMyMind.html', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/ReadMyMind.html');
    con.query('INSERT IGNORE INTO RMM SET ?', {
        ip: req.connection.remoteAddress.toString(),
        best: 0,
        network_best: 0
    }, function (err, rows) {
        if (err) console.log(err);
    });
});

app.get('/ReadMyMind.jpg', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/ReadMyMind.jpg');
});

app.get('/ReadMyMind.css', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/ReadMyMind.css');
});

app.get('/ReadMyMind.js', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/ReadMyMind.js');
});

app.get('/SPS.html', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/SPS.html');
    con.query('INSERT IGNORE INTO SPS SET ?', {
        ip: req.connection.remoteAddress.toString(),
        best: 0,
        network_best: 0
    }, function (err, rows) {
        if (err) console.log(err);
    });
});

app.get('/SPS.jpg', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/SPS.jpg');
});

app.get('/SPS.css', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/SPS.css');
});

app.get('/SPS.js', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/SPS.js');
});

app.get('/Stone.png', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Stone.png');
});

app.get('/Paper.jpg', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Paper.jpg');
});

app.get('/Scissors.jpg', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/Scissors.jpg');
});

app.get('/img1.jpeg', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/img1.jpeg');
});

app.get('/img2.jpg', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/img2.jpg');
});

app.get('/img3.jpeg', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/img3.jpeg');
});

app.get('/img4.jpg', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/img4.jpg');
});

app.get('/img5.jpg', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/img5.jpg');
});

app.get('/img6.jpeg', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/img6.jpeg');
});

app.get('/img7.jpg', function (req, res) {
    Console(req);
    res.sendFile(__dirname + '/img7.jpg');
});

app.get('/myDetails', function (req, res) {
    con.query('SELECT * FROM Users WHERE ip = ?', req.connection.remoteAddress, function (err, rows) {
        if (err) console.log(err);
        else {
            res.json({
                name: rows[0].name,
                best: rows[0].best,
                network_best: rows[0].network_best
            });
        }
    });
});

app.get('/best', function (req, res) {
    con.query('SELECT * FROM Users WHERE ip = ?', "best", function (err, rows) {
        if (err) console.log(err);
        else {
            res.json({
                best: rows[0].best,
                name: rows[0].name
            });
        }
    });
});

app.get('/setName/:name', function (req, res) {
    con.query('UPDATE Users SET name = ? WHERE ip = ?', [req.params.name, req.connection.remoteAddress], function (err, rows) {
        if (err) console.log(err);
    });
});

app.get('/setBest/:best', function (req, res) {
    con.query('UPDATE Users SET best = ? WHERE ip = ?', [req.params.best, req.connection.remoteAddress], function (err, rows) {
        if (err) console.log(err);
    });
});

app.get('/setNetworkBest/:best', function (req, res) {
    con.query('SELECT * FROM Users WHERE network_best = ?', '1', function (err, rows) {
        console.log(rows);
        if (err) console.log(err);
        else {
            con.query('UPDATE Users SET network_best = ? WHERE ip = ?', ['0', rows[0].ip], function (err, result) {
                console.log(result);
                if (err) console.log(err);
            });
            con.query('UPDATE Users SET best = ?, network_best = ? WHERE ip = ?', [req.params.best, '1', req.connection.remoteAddress], function (err, result) {
                console.log(result);
                if (err) console.log(err);
                else {
                    con.query('SELECT * FROM Users WHERE ip = ?', req.connection.remoteAddress, function (err, rows) {
                        console.log(rows);
                        if (err) console.log(err);
                        else {
                            con.query('UPDATE Users SET best = ?, name = ? WHERE ip = ?', [req.params.best, rows[0].name, "best"], function (err, result) {
                                console.log(result);
                                if (err) console.log(err);
                            });
                        }
                    });
                }
            });
        }
    });
});

app.get('/SPSGetMyDetails', function (req, res) {
    con.query('SELECT * FROM SPS WHERE ip = ?', req.connection.remoteAddress, function (err, rows) {
        if (err) console.log(err);
        else {
            res.json({
                name: rows[0].name,
                best: rows[0].best,
                network_best: rows[0].network_best
            });
        }
    });
});

app.get('/SPSGetBest', function (req, res) {
    con.query('SELECT * FROM SPS WHERE ip = ?', "best", function (err, rows) {
        if (err) console.log(err);
        else {
            res.json({
                best: rows[0].best,
                name: rows[0].name
            });
        }
    });
});

app.get('/SPSSetName/:name', function (req, res) {
    con.query('UPDATE SPS SET name = ? WHERE ip = ?', [req.params.name, req.connection.remoteAddress], function (err, rows) {
        if (err) console.log(err);
    });
});

app.get('/SPSSetScore/:best', function (req, res) {
    con.query('UPDATE SPS SET best = ? WHERE ip = ?', [req.params.best, req.connection.remoteAddress], function (err, rows) {
        if (err) console.log(err);
    });
});

app.get('/SPSSetBest/:best', function (req, res) {
    con.query('SELECT * FROM SPS WHERE network_best = ?', '1', function (err, rows) {
        console.log(rows);
        if (err) console.log(err);
        else {
            con.query('UPDATE SPS SET network_best = ? WHERE ip = ?', ['0', rows[0].ip], function (err, result) {
                console.log(result);
                if (err) console.log(err);
            });
        }
        con.query('UPDATE SPS SET best = ?, network_best = ? WHERE ip = ?', [req.params.best, '1', req.connection.remoteAddress], function (err, result) {
            console.log(result);
            if (err) console.log(err);
            else {
                con.query('SELECT * FROM SPS WHERE ip = ?', req.connection.remoteAddress, function (err, rows) {
                    console.log(rows);
                    if (err) console.log(err);
                    else {
                        con.query('UPDATE SPS SET best = ?, name = ? WHERE ip = ?', [req.params.best, rows[0].name, "best"], function (err, result) {
                            console.log(result);
                            if (err) console.log(err);
                        });
                    }
                });
            }
        });
    });
});

app.get('/RMMGetMyDetails', function (req, res) {
    con.query('SELECT * FROM RMM WHERE ip = ?', req.connection.remoteAddress, function (err, rows) {
        if (err) console.log(err);
        else {
            res.json({
                name: rows[0].name,
                best: rows[0].best,
                network_best: rows[0].network_best
            });
        }
    });
});

app.get('/RMMGetBest', function (req, res) {
    con.query('SELECT * FROM RMM WHERE ip = ?', "best", function (err, rows) {
        if (err) console.log(err);
        else {
            res.json({
                best: rows[0].best,
                name: rows[0].name
            });
        }
    });
});

app.get('/RMMSetName/:name', function (req, res) {
    con.query('UPDATE RMM SET name = ? WHERE ip = ?', [req.params.name, req.connection.remoteAddress], function (err, rows) {
        if (err) console.log(err);
    });
});

app.get('/RMMSetScore/:best', function (req, res) {
    con.query('UPDATE RMM SET best = ? WHERE ip = ?', [req.params.best, req.connection.remoteAddress], function (err, rows) {
        if (err) console.log(err);
    });
});

app.get('/RMMSetBest/:best', function (req, res) {
    con.query('SELECT * FROM RMM WHERE network_best = ?', '1', function (err, rows) {
        console.log(rows);
        if (err) console.log(err);
        else {
            con.query('UPDATE RMM SET network_best = ? WHERE ip = ?', ['0', rows[0].ip], function (err, result) {
                console.log(result);
                if (err) console.log(err);
            });
        }
        con.query('UPDATE RMM SET best = ?, network_best = ? WHERE ip = ?', [req.params.best, '1', req.connection.remoteAddress], function (err, result) {
            console.log(result);
            if (err) console.log(err);
            else {
                con.query('SELECT * FROM RMM WHERE ip = ?', req.connection.remoteAddress, function (err, rows) {
                    console.log(rows);
                    if (err) console.log(err);
                    else {
                        con.query('UPDATE RMM SET best = ?, name = ? WHERE ip = ?', [req.params.best, rows[0].name, "best"], function (err, result) {
                            console.log(result);
                            if (err) console.log(err);
                        });
                    }
                });
            }
        });
    });
});

app.get('/PM/:imgName', function (req, res) {
    con.query('SELECT * FROM PM WHERE img = ?', req.params.imgName, function (err, rows1) {
        console.log(rows1);
        if (err) console.log(err);
        else {
            con.query('UPDATE PM SET count = ? WHERE img = ?', [rows1[0].count + 1, rows1[0].img], function (err, result1) {
                console.log(result1);
                if (err) console.log(err);
                else {
                    con.query('SELECT * FROM PM WHERE img = ?', 'best', function (err, rows2) {
                        console.log(rows2);
                        if (err) console.log(err);
                        else {
                            con.query('UPDATE PM SET count = ? WHERE img = ?', [rows2[0].count + 1, 'best'], function (err, result2) {
                                console.log(result2);
                                if (err) console.log(result2);
                                else {
                                    res.json({
                                        count: rows1[0].count + 1,
                                        total: rows2[0].count + 1
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
});

app.listen(8000, function () {
    console.log('Express server listening on port 8000\n');
});
