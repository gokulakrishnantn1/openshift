var num;
var input;
var correct;
var count;
var message, lastMsg;

var name, best, champion, overallBest, championName;

$.get("/RMMGetBest", function (data1, status) {
    overallBest = data1.best;
    championName = data1.name;
    $.get("/RMMGetMyDetails", function (data, status) {
        name = data.name;
        best = data.best;
        champion = data.network_best;
        if (data.network_best && data.name != "") {
            $('.modal-title').html("<p>Welcome Champion !</p>");
            $('.modal-body').html("<p>Guess the number with the feedbacks received</p><br><p>Your best : " + best + "</p>");
        } else if (data.name) {
            $('.modal-title').html("<p>Welcome " + data.name + "</p>");
            $('.modal-body').html("<p>Guess the number with the feedbacks received</p><br><p>Your best : " + best + "</p><br><p>Network Best : " + overallBest + "</p><br><p>Current Champion : " + championName + "</p>");
        } else {
            name = prompt("Enter your name");
            if (name == "" || name == "null") {
                $('.modal-title').html("<p>Welcome Guest</p>");
                $('.modal-body').html("<p>Guess the number with the feedbacks received</p><br><p>Your best : " + best + "</p><br><p>Network Best : " + overallBest + "</p><br><p>Current Champion : " + championName + "</p>");
            } else {
                $.get("/RMMSetName/" + name);
                $('.modal-title').html("<p>Welcome " + name + "</p>");
                $('.modal-body').html("<p>Guess the number with the feedbacks received</p><br><p>Your best : " + best + "</p><br><p>Network Best : " + overallBest + "</p><br><p>Current Champion : " + championName + "</p>");
            }
        }
        $('#controls').modal();
    });
});

var start = function () {
    document.getElementById("result").style.display = "none";
    document.getElementById("again").style.display = "none";
    document.getElementById("guessAgain").style.display = "none";
    document.getElementById("message").style.display = "";
    document.getElementById("input").style.display = "";
    document.getElementById("btn").style.display = "";
    document.getElementById("input").value = "";

    num = Math.floor(Math.random() * 100) + 1;
    document.getElementById("message").innerHTML = "I'm thinking of an integer between 1 and 100. Can you guess it ?";
    correct = false;
    count = 0;
    lastMsg = "";
    message = "";
}

//Initialisation
document.getElementById("result").style.display = "none";
document.getElementById("again").style.display = "none";
document.getElementById("guessAgain").style.display = "none";

var clicked = function () {
    lastMsg = message;
    input = document.getElementById("input").value;
    count++;
    if (input == "")
        correct = true;
    else if (isNaN(input))
        message = "Is that a number !?";
    else if (input > 100 || input < 0)
        message = "Oops..! Enter an integer between 1 and 100";
    else if (input % 1 != 0)
        message = "I don't think it is an integer";
    else if (input > num)
        message = "It is higher than what I thought";
    else if (input < num)
        message = "It is less than what I thought";
    else {
        document.getElementById("result").innerHTML = "Great ! You have made it.\n\nIt took " + count + " guesses to make it.";
        correct = true;
        if (count < best || best == 0) {
            document.getElementById("best").innerHTML = "Best : " + count;
            best = count;
        }
        $.get("/RMMGetBest", function (data, status) {
            overallBest = data.best;
            if (Number(overallBest) > Number(count) && name != "" && name != "null") {
                $('.modal-title').html("<p>Congrats ! Champion !</p>");
                $('.modal-body').html("<p>You have set a new network best : " + count + "seconds</p>");
                $.get("/RMMSetBest/" + count);
                $.get("/RMMSetScore/" + count);
            } else if (Number(best) > Number(count) || Number(best) == 0) {
                $('.modal-title').html("<p>Congrats !</p>");
                $('.modal-body').html("<p>You have set a new personal best : " + count + "</p><p>Network Best : " + overallBest + "</p>");
                $.get("/RMMSetScore/" + count);
            } else {
                $('.modal-title').html("<p>Congrats !</p>");
                $('.modal-body').html("<p>You have completed in " + count + " guesses</p><p>Your Best : " + best + "</p><p>Network Best : " + overallBest + "</p>");
            }
            $('#controls').modal();
        });
        document.getElementById("result").style.display = "";
        document.getElementById("again").style.display = "";
        document.getElementById("guessAgain").style.display = "";
        document.getElementById("message").style.display = "none";
        document.getElementById("input").style.display = "none";
        document.getElementById("btn").style.display = "none";
    }
    if (!correct) {
        if (lastMsg == message)
            message = "Still the same";
        document.getElementById("message").innerHTML = message;
    }
}

$(window).resize(function () {
    if ($(window).width() / 1280 < $(window).height() / 934) {
        document.body.style.zoom = "" + ($(window).width() / 1280) * 100 + "%";
    } else {
        document.body.style.zoom = "" + ($(window).height() / 934) * 100 + "%";
    }
});

if ($(window).width() != 1280 || $(window).height() != 934)
    if ($(window).width() / 1280 < $(window).height() / 934) {
        document.body.style.zoom = "" + ($(window).width() / 1280) * 100 + "%";
    } else {
        document.body.style.zoom = "" + ($(window).height() / 934) * 100 + "%";
    }
