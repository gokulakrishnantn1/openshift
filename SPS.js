var score1 = 0,
    score2 = 0,
    overallBest = 0,
    championName, name, best = 0,
    champion, attempt = 0;

$.get("/SPSGetBest", function (data1, status) {
    overallBest = data1.best;
    championName = data1.name;
    $.get("/SPSGetMyDetails", function (data, status) {
        name = data.name;
        best = data.best;
        champion = data.network_best;
        if (data.network_best && data.name != "") {
            $('.modal-title').html("<p>Welcome Champion !</p>");
            $('.modal-body').html("<p>You have 20 tries to show your best</p><br><p>Your best : " + best + "</p>");
        } else if (data.name) {
            $('.modal-title').html("<p>Welcome " + data.name + "</p>");
            $('.modal-body').html("<p>You have 20 tries to show your best</p><br><p>Your best : " + best + "</p><br><p>Network Best : " + overallBest + "</p><br><p>Current Champion : " + championName + "</p>");
        } else {
            name = prompt("Enter your name");
            if (name == "" || name == "null") {
                $('.modal-title').html("<p>Welcome Guest</p>");
                $('.modal-body').html("<p>You have 20 tries to show your best</p><br><p>Your best : " + best + "</p><br><p>Network Best : " + overallBest + "</p><br><p>Current Champion : " + championName + "</p>");
            } else {
                $.get("/SPSSetName/" + name);
                $('.modal-title').html("<p>Welcome " + name + "</p>");
                $('.modal-body').html("<p>You have 20 tries to show your best</p><br><p>Your best : " + best + "</p><br><p>Network Best : " + overallBest + "</p><br><p>Current Champion : " + championName + "</p>");
            }
        }
        $('#controls').modal();
    });
});

function execute(input) {
    attempt++;
    var rand = Math.floor((Math.random() * 3) + 1);
    switch (rand) {
        case 1:
            $('#lastComp').attr("src", "Stone.png");
            break;
        case 2:
            $('#lastComp').attr("src", "Paper.jpg");
            break;
        case 3:
            $('#lastComp').attr("src", "Scissors.jpg");
    }
    switch (input) {
        case "1":
            if (rand == 2)
                score2++;
            else if (rand == 3)
                score1++;
            $('#lastUser').attr("src", "Stone.png");
            break;
        case "2":
            if (rand == 3)
                score2++;
            else if (rand == 1)
                score1++;
            $('#lastUser').attr("src", "Paper.jpg");
            break;
        case "3":
            if (rand == 1)
                score2++;
            else if (rand == 2)
                score1++;
            $('#lastUser').attr("src", "Scissors.jpg");
    }
    document.getElementById("score1").innerHTML = score1;
    document.getElementById("score2").innerHTML = score2;

    if (attempt == 20) {
        $.get("/SPSGetBest", function (data, status) {
            $('body').html('<div class="modal fade" id="controls" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h3 class="modal-title"></h3></div><div class="modal-body"></div></div></div></div>');
            overallBest = data.best;
            if (Number(overallBest) < Number(score1) && name != "" && name != "null") {
                $('.modal-title').html("<p>Congrats ! Champion !</p>");
                $('.modal-body').html("<p>You have set a new network best : " + score1 + "seconds</p>");
                $.get("/SPSSetBest/" + score1);
                $.get("/SPSSetScore/" + score1);
            } else if (Number(best) < Number(score1) || Number(best) == 0) {
                $('.modal-title').html("<p>Congrats !</p>");
                $('.modal-body').html("<p>You have set a new personal best : " + score1 + "</p><p>Network Best : " + overallBest + "</p>");
                $.get("/SPSSetScore/" + score1);
            } else {
                $('.modal-title').html("<p>Congrats !</p>");
                $('.modal-body').html("<p>You have scored " + score1 + " points</p><p>Your Best : " + best + "</p><p>Network Best : " + overallBest + "</p>");
            }
            $('#controls').modal();
        });
    }

};

$(window).resize(function () {
    if ($(window).width() / 1280 < $(window).height() / 934) {
        document.body.style.zoom = "" + ($(window).width() / 1280) * 100 + "%";
    } else {
        document.body.style.zoom = "" + ($(window).height() / 934) * 100 + "%";
    }
});

if ($(window).width() != 1280 || $(window).height() != 934)
    if ($(window).width() / 1280 < $(window).height() / 934) {
        document.body.style.zoom = "" + ($(window).width() / 1280) * 100 + "%";
    } else {
        document.body.style.zoom = "" + ($(window).height() / 934) * 100 + "%";
    }
